
# String Compression
def compress(s: str):
    result = ""
    if s == "":
        return result
    count = 1
    # Initialize start and end pointers to keep track of consecutive characters in s
    start = 0
    end = 1
    while end < len(s):
        if s[end] != s[start]:
            #Add character and number of consecutive appreances to the result
            result += s[start] + str(count)
            #Set start to the index of the new character and reset count
            start = end
            count = 1
        else:
            #Add number of appearances of s[start]
            count += 1
        end += 1
    
    # Add last character and count to the result
    result += s[start] + str(count)
    return result

# The time complexity of compress is O(n) with n being the length of s because it iterates over the entire length of s
# in the while loop.


# Directed Graph data structure
class DiGraph:
    def __init__(self):
        # Stores the values of nodes being connected by inbound links in a list to each node 1-6
        self.incoming_edges = [[] for _ in range(6)]
        # Stores the values of nodes being connected by outbound links in a list to each node 1-6 
        self.outgoing_edges = [[] for _ in range(6)]
    
    def get_incoming_edges(self, n):
        # Returns all nodes connected by inbound links for node n
        return self.incoming_edges[n-1]
    
    def get_outgoing_edges(self, n):
        # Returns all nodes connected by outbound links for node n
        return self.outgoing_edges[n-1]
    
    def add_edge(self, v1, v2):
        # Checks to see if connection has already been recorded 
        if v2 not in self.outgoing_edges[v1-1] and v1 not in self.incoming_edges[v2-1]:
            # Adds outbound link to v1 and inbound link to v2
            self.outgoing_edges[v1-1].append(v2)
            self.incoming_edges[v2-1].append(v1)


class ListNode:
    # Linked List Node consisting of a value and a next field pointing to the next node in the linked list
    def __init__(self, val):
        self.val = val
        self.next = None

# Assumes that input is giving as linked list, with edges containing the node with the first value in the linked list
def identify_router(edges: ListNode):
    dG = DiGraph()
    while edges.next:
        # Builds directed graph
        dG.add_edge(edges.val, edges.next.val)
        edges = edges.next

    solution = []
    max_edges = 0

    for i in range(1, 7):
        # Obtain inbound links and outbound links for each node and sum lengths together to obtain total number of
        # connections
        incoming, outgoing = get_incoming_edges(i), get_outgoing_edges(i)
        total = len(incoming) + len(outgoing)

        if total>max_edges:
            # If node has most connections so far, rewrite solution to contain only label of this node and reset
            # max_edges
            solution = [i]
            max_edges = total
        elif total == max_edges:
            # Add node value to solution as it has a tie for the largest number of connections
            solution.append(i)
    
    return solution

# Since the number of routers is always guaranteed to be 6, the time complexity of identify_router is O(1). If the
# number of routers is variable, then building the graph would take O(E^2) time where E is the number of edges in
# the graph, since the while loop at line 65 iterates over all edges in O(E) and the add_edge function inside
# the loop takes O(E) time becauses it checks over all the recorded edges to see if an edge has already been recorded.
# The for loop in identify_router would then take O(V) time, where V is the number of vertices in the graph, because it
# iterates over all the vertices. Therefore, if the number of routers is variable, the time complexity is O(V*E^2).
